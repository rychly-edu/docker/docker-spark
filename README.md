# Apache Spark Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-spark/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-spark/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-spark/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-spark/commits/master)

The image is based on [rychly-docker/docker-hadoop-base](/rychly-edu/docker/docker-hadoop-base).
The version of the base image can be restricted on build by the `HADOOP_VERSION` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-spark:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-spark" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run by Docker-Compose

See [docker-compose.yml](docker-compose.yml).

Use the `ROLE` environment variable or the `entrypoint-<role>.sh` script to start the Spark node in a particular role.
The role has to be one of the following:

*	`master` -- for a master node
*	`worker` -- for a worker/slave node
*	`history` -- for a history server node

## Spark History Server

### Running NameNode Required

A HDFS namenode must be running and accessible on the Spark History Server start-up.
The Spark History Server image can wait for the namenode by setting environment variable `WAIT_FOR`, e.g., to `namenode:8020`.

### Log Directory in HDFS

The Spark log directory can be set by `LOG_DIR` environment variable, e.g., to `hdfs://namenode:8020/user/spark/applicationHistory`.
In the case of the log directory in an HDFS, the log directory will be created in the HDFS as writable by "spark" user.
