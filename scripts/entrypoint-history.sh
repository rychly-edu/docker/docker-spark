#!/bin/sh

DIR=$(dirname "${0}")

# For Spark properties, see
# https://spark.apache.org/docs/latest/configuration.html
# https://spark.apache.org/docs/latest/monitoring.html#spark-configuration-options

. "${DIR}/spark-entrypoint-helpers.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

set_histui_port
set_log_dir

if [ -z "${LOG_DIR}" ]; then
	echo "Variable 'LOG_DIR', 'PROP_SPARK_spark_history_fs_logDirectory', or 'PROP_SPARK_spark_eventLog_dir' has to be set correctly!" >&2
	exit 1
fi

. "${DIR}/hadoop-set-props.sh"
. "${DIR}/spark-set-props.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
wait_for
make_log_dir

exec su "${SPARK_USER}" -c "exec ${SPARK_HOME}/bin/spark-class org.apache.spark.deploy.history.HistoryServer ${@}"
