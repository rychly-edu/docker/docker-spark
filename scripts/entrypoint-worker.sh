#!/bin/sh

DIR=$(dirname "${0}")

# For Spark properties, see
# https://spark.apache.org/docs/latest/configuration.html

. "${DIR}/spark-entrypoint-helpers.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

if [ -n "${PROP_SPARK_spark_master}" ]; then
	export MASTER_URL="${PROP_SPARK_spark_master}"
elif [ -n "${MASTER_URL}" ]; then
	export PROP_SPARK_spark_master="${MASTER_URL}"
else
	echo "Variable 'MASTER_URL' or 'PROP_SPARK_spark_master' has to be set correctly!" >&2
	exit 1
fi

set_webui_port
set_histui_port
set_log_dir
expand_spark_jars_dirs

. "${DIR}/hadoop-set-props.sh"
. "${DIR}/spark-set-props.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
wait_for

exec su "${SPARK_USER}" -c "exec ${SPARK_HOME}/bin/spark-class org.apache.spark.deploy.worker.Worker ${MASTER_URL} --webui-port ${WEBUI_PORT:-4040} ${@}"
