#!/bin/sh

DIR=$(dirname "${0}")

# For Spark properties, see
# https://spark.apache.org/docs/latest/configuration.html

. "${DIR}/spark-entrypoint-helpers.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

if [ -n "${PROP_SPARK_spark_master}" ]; then
	export MASTER_PORT="${PROP_SPARK_spark_master##*:}"
elif [ -n "${MASTER_PORT}" ]; then
	export PROP_SPARK_spark_master="spark://$(hostname -f):${MASTER_PORT}"
fi

set_webui_port
set_histui_port
set_log_dir
expand_spark_jars_dirs

. "${DIR}/hadoop-set-props.sh"
. "${DIR}/spark-set-props.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
wait_for
make_hdfs_dirs

exec su "${SPARK_USER}" -c "exec ${SPARK_HOME}/bin/spark-class org.apache.spark.deploy.master.Master --port ${MASTER_PORT:-7077} --webui-port ${WEBUI_PORT:-4040} ${@}"
