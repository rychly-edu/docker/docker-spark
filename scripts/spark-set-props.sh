#!/bin/sh

. $(dirname "${0}")/set-pax-flag-of-java.sh

# the functions require GNU sed (the usage of busybox sed may result into incorrect outputs)

addSparkProperty() {
	local path="${1}"
	local name="${2}"
	local value="${3}"
	echo "${name}	${value}" >> "${path}"
}

cleanSparkProperties() {
	local path="${1}"
	[ -e "${path}" ] && sed -i '/^[^#]/d' "${path}"
}

reconfSparkByEnvVars() {
	local path="${1}"
	local envPrefix="${2}"
	echo "* reconfiguring ${path}"
	cleanSparkProperties "${path}"
	for I in `printenv | grep "^${envPrefix}_[^=]*="`; do
		local name=`echo "${I}" | sed -e "s/^${envPrefix}_\\([^=]*\\)=.*\$/\\1/" -e 's/___/-/g' -e 's/__/_/g' -e 's/_/./g'`
		local value="${I#*=}"
		echo "** setting ${name}=${value}"
		addSparkProperty "${path}" "${name}" "${value}"
	done
}

reconfSparkByEnvVars "${SPARK_DEF_CONF}" PROP_SPARK
